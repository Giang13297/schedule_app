from django.shortcuts import render
from .models import Works, Device
from firebase_admin.messaging import Message, Notification
from fcm_django.models import FCMDevice
from rest_framework.views import APIView
from .serializers import GetscheduleSerializer, DeviceSerializer
from rest_framework.response import Response
from rest_framework import status


# Create your views here.

def viewwork(request):
    w = Works.objects.all()
    list_w = {'List_works': w}
    return render(request, 'notification_app/view.html', list_w)


class GetAPIView(APIView):
    def get(self, request):
        list_work = Works.objects.all()
        mydata = GetscheduleSerializer(list_work, many=True)
        return Response(data=mydata.data, status=status.HTTP_200_OK, content_type="application/json; charset=utf-8")


class PostAPIView(APIView):
    def post(self, request):
        mydata = DeviceSerializer(data=request.data)
        if not mydata.is_valid():
            return Response("Wrong Token", status=status.HTTP_400_BAD_REQUEST)
        else:
            print("Received Token")
            device_token = mydata.data['device_token']
            token = FCMDevice.objects.create(registration_id=device_token)
            return Response(data=token.id, status=status.HTTP_200_OK)
