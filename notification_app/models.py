# from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from fcm_django.models import FCMDevice
# Create your models here.

class Works(models.Model):
    name = models.TextField(max_length=100)
    content = models.TextField(max_length=300)
    time_start = models.DateTimeField(default=timezone.datetime.now())
    # time_start = time_start.strftime("%d-%m-%Y %H:%M:%S")
    time_end = models.DateTimeField(default=timezone.datetime.now())
    # time_end= time_end.strftime("%d-%m-%Y %H:%M:%S")


class Device(models.Model):
    device_token = models.TextField(max_length=300)

