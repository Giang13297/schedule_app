from django.urls import path
from . import views


urlpatterns = [
    path('', views.viewwork, name="view"),
    path('get/', views.GetAPIView.as_view()),
    path('post/', views.PostAPIView.as_view()),
]