from rest_framework import serializers
from .models import Works


class GetscheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Works
        fields = ('name', 'content', 'time_start', 'time_end')


class DeviceSerializer(serializers.Serializer):
    device_token = serializers.CharField(max_length=300)