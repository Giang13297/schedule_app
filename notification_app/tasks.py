from __future__ import absolute_import, unicode_literals
from celery import shared_task, Celery
from celery.utils.log import get_task_logger
from celery.schedules import crontab
from firebase_admin.messaging import Notification, Message
from .models import Works, FCMDevice
import datetime as dt
from datetime import datetime


from .models import Works

app = Celery()

logger = get_task_logger(__name__)


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('hello') every 10 seconds.
    sender.add_periodic_task(crontab(minute=1), calculate.s())


@app.task()
def calculate():
    # logger("aaaaa")
    # return 'xyz'
    # # print("a")
    list_work = Works.objects.all()
    for i in range(0, len(list_work)):
        compare_time = str(list_work[i].time_start)[:-6]
        compare_time = datetime.strptime(compare_time, '%Y-%m-%d %H:%M:%S')
        time_now = dt.datetime.now()
        delta_time = compare_time - time_now
        # print(delta_time.seconds)
        # print(compare_time)
        # print(time_now)
        # print("Done")
        if 120 < delta_time.seconds < 3600:
            content = list_work[i].content
            device = FCMDevice.objects.all().first()
            token = device.registration_id
            noti = Notification(title="Sắp đến giờ", body=content)
            message = Message(token=token, notification=noti)
            device.send_message(message)
            # print(content)
            # rdb.set_trace()

    return 'Done'


# def sendfcm():
#     device = FCMDevice.objects.all().first()
#     token = device.registration_id
#     noti = Notification(title="Sắp đến giờ", body=calculate())
#     message = Message(token=token, notification=noti)
#     device.send_message(message)
#     return print("Send Successfully!")
