from django.contrib import admin
from .models import Works, Device
# Register your models here.
admin.site.register(Works)
admin.site.register(Device)