# Generated by Django 3.2.6 on 2021-08-29 09:12

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notification_app', '0009_auto_20210829_1441'),
    ]

    operations = [
        migrations.AlterField(
            model_name='works',
            name='time_end',
            field=models.DateTimeField(default=datetime.datetime(2021, 8, 29, 16, 12, 28, 822927)),
        ),
        migrations.AlterField(
            model_name='works',
            name='time_start',
            field=models.DateTimeField(default=datetime.datetime(2021, 8, 29, 16, 12, 28, 822927)),
        ),
    ]
