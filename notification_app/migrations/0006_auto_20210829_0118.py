# Generated by Django 3.2.6 on 2021-08-28 18:18

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notification_app', '0005_auto_20210829_0117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='works',
            name='time_end',
            field=models.DateTimeField(default=datetime.datetime(2021, 8, 29, 1, 18, 55, 725771)),
        ),
        migrations.AlterField(
            model_name='works',
            name='time_start',
            field=models.DateTimeField(default=datetime.datetime(2021, 8, 29, 1, 18, 55, 725771)),
        ),
    ]
